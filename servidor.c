#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define MAX_BUF 1024
#define PORT 50005

int main()
{
	int sock, n, i, j, k;             //indentificador del socket y variables para controlar bucles.
	int num_clientes = 0;             //numero de clientes que se ir�n conectando.
	char lista_de_nombres[20][40];    //lista donde se ir�n guardando los nombres de los usuarios.
	char lista_concat[MAX_BUF];       //En esta lista concatenaremos todos los nombres de los usuarios.
	char lista_de_ip[10][15];         //lista donde se ir�n guardando las direcciones ip de los clientes. 
	char listaDir_concat[MAX_BUF];    //En esta lista concatenaremos todas las direcciones ip de los clientes.
	char lista_de_puertos[10][15];    //lista donde ser ir�n guardando los puertos de los clientes. 
	char listaPuer_concat[MAX_BUF];   //En esta lista concatenaremos todos puertos de los clientes.
	char buf[MAX_BUF];                //Esta variable la usaremos para recibir todo lo que nos env�e un cliente.
	char comando[5];	                //Aqui guardaremos el comando que nos env�e el cliente.
	char nombre[10];                  //variable que usaremos para recibir el nombre de un usuario que desea desconectarse.
	char menu[MAX_BUF]; 	             //men� concatenado que enviaremos al cliente.
  	char *dir;                        //variable para guardar la ip del cliente.
  	socklen_t tam_dir;
	struct sockaddr_in dir_serv, dir_cli;   
	
	
	//inicializamos todo
	for (i=0; i< 20;i++){
		memset(lista_de_nombres[i],'\0',40);
	}
	memset(lista_concat,'\0',MAX_BUF);
	
	for (i=0; i< 10;i++){
	   	memset(lista_de_ip[i],'\0',15);
	   	memset(lista_de_puertos,'\0',15);
	}
	memset(listaDir_concat,'\0',MAX_BUF);
	memset(listaPuer_concat,'\0',MAX_BUF);
	memset(buf,'\0',MAX_BUF);

	if((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)	{
		perror("Error al crear el socket.\n");
		exit(1);
	}	
	memset(&dir_serv, 0, sizeof(dir_serv));
	dir_serv.sin_family = AF_INET;
	dir_serv.sin_addr.s_addr = htonl(INADDR_ANY);  
	dir_serv.sin_port = htons(PORT);	
	
	if(bind(sock, (struct sockaddr *) &dir_serv, sizeof(dir_serv)) < 0)
	{
		perror("Error al asignarle una direccion al socket.\n");
		exit(1);
	}
	while(1)
	{   
		memset(comando,'\0',5);
		tam_dir = sizeof(dir_cli);
		if((n=recvfrom(sock, buf, MAX_BUF, 0, (struct sockaddr *) &dir_cli, &tam_dir)) < 0)
		{
			perror("Error al recibir datos.\n");
			exit(1);
		}
		buf[n]=0;
		strncpy(comando,buf,4);
		comando[5] = 0;
		i=5;

						  	/****************************
    				           			CONECTAR
    							*****************************/    

		if (strcmp(comando, "CONE") == 0){	
			// guardamos el nombre de los los clientes en una lista.

			for (j = 0; i < n; i++){
				if(buf[i] == '&'){ i++;break;}
					lista_de_nombres[num_clientes][j] = buf[i];
					j++;
			}

			//guardamos el puerto de los clientes en una lista.
			strcpy(lista_de_puertos[num_clientes], buf+i);
			lista_de_nombres[num_clientes][j-1]=0;
			lista_de_puertos[num_clientes][k-1]=0;

			//impresi�n por pantalla del jugador conectado.
			printf("Se acaba de conectar el jugador : %s\n",lista_de_nombres[num_clientes]);
			
 			//guardamos la ip del cliente en una lista. 			
	 		dir = inet_ntoa(dir_cli.sin_addr);
	 		sprintf(lista_de_ip[num_clientes],"%s", dir);
			num_clientes++;

			//creaci�n del men�, separado por "&".				
			if(sprintf(menu,"OK_Ver la lista de jugadores&Cerrar programa&Recargar menu") < 0){
				fprintf(stderr, "Error en la conversi�n del comando a enviar.\n");
				exit(1);
			}					
			//envio del OK concatenado con toda la estructura del men�
			if(sendto(sock, menu, strlen(menu), 0, (struct sockaddr *) &dir_cli, tam_dir) < 0){
				perror("Error al enviar datos.\n");
				exit(1);
			}

							/****************************
    							      LISTA DE JUGADORES
    							*****************************/
    		}else if(strcmp(comando, "LIST") == 0){
			
			//vaciamos siempre el contenido de la lista
			memset(lista_concat,'\0',MAX_BUF);	
		
			//concatenaci�n de los nombres de la lista 
			sprintf(lista_concat,"OK_");
			k = strlen(lista_concat);
			for( i = 0 ; i < num_clientes; i ++){
				n=strlen(lista_de_nombres[i]);
				for (j = 0; j < n ; j ++){
					if ( lista_de_nombres[i][j] != 0)	{
						lista_concat[k] = lista_de_nombres[i][j];
						k++;
					}
				}				
				if ( num_clientes > i+1){
					lista_concat[k] = '&';
					k++;
				}
			}
			//concatenaci�n de las direcciones de la lista
			sprintf(listaDir_concat,"OK_");
			k = strlen(listaDir_concat);			
			for( i = 0 ; i < num_clientes; i ++){
				for (j = 0; j < strlen(lista_de_ip[i]) ; j ++){
					if ( (int)lista_de_ip[i][j] != 10)	{
						listaDir_concat[k] = lista_de_ip[i][j];
						k++;
					}
				}				
				if ( num_clientes > i+1){
					listaDir_concat[k] = '&';
					k++;
				}
			}				
			//concatenaci�n de los puertos de la lista
			sprintf(listaPuer_concat,"OK_");
			k=strlen(listaPuer_concat);
			for( i = 0 ; i < num_clientes; i ++){
				for (j = 0; j < strlen(lista_de_puertos[i]) ; j ++){
					if ( (int)lista_de_puertos[i][j] != 10)	{
						listaPuer_concat[k] = lista_de_puertos[i][j];
						k++;
					}
				}				
				if ( num_clientes > i+1){
					listaPuer_concat[k] = '&';
					k++;
				}
			}			
			
			//Envio de respuesta al cliente con las listas concatenadas, una por una, despu�s de cada 
			//env�o el cliente enviar� una respuesta de OK, lo cual significar� que se puede enviar la siguiente lista.	
			if(sendto(sock, lista_concat, strlen(lista_concat), 0, (struct sockaddr *) &dir_cli, tam_dir) < 0){
				perror("Error al enviar datos.\n");
				exit(1);
			}
			//respuesta del cliente				
			if((n=recvfrom(sock, buf, MAX_BUF, 0, (struct sockaddr *) &dir_cli, &tam_dir)) < 0){
				perror("Error al recibir datos.\n");
				exit(1);
		        }
			buf[n]=0;
			if(buf[0] == 'O' && buf[1] == 'K'){
				if(sendto(sock, listaDir_concat, strlen(listaDir_concat), 0, (struct sockaddr *) &dir_cli, tam_dir) < 0){
					perror("Error al enviar datos.\n");
					exit(1);
			    	}	
			}
			//respuesta del cliente			 		
			if((n=recvfrom(sock, buf, MAX_BUF, 0, (struct sockaddr *) &dir_cli, &tam_dir)) < 0){
				perror("Error al recibir datos.\n");
				exit(1);
			}
		        buf[n]=0;			
			if(buf[0] == 'O' && buf[1] == 'K'){
				if(sendto(sock, listaPuer_concat, strlen(listaPuer_concat), 0, (struct sockaddr *) &dir_cli, tam_dir) < 0){
					perror("Error al enviar datos.\n");
					exit(1);
			  	}	
			}
			//respuesta del cliente			
			if((n=recvfrom(sock, buf, MAX_BUF, 0, (struct sockaddr *) &dir_cli, &tam_dir)) < 0){
				perror("Error al recibir datos.\n");
				exit(1);
		   	}
		        buf[n]=0;			
		        if(buf[0] == 'E' && buf[1] == 'R'){
		        	fprintf(stderr, "Error.\n");
		        	exit(1);			  
			}		  			
			
							/****************************
    							        Desconectarse
    							*****************************/					
		}else if(strcmp(comando, "EXIT") == 0){			
			for (j = 0; i < n; i++){
				if((int)buf[i] == 10){break;}
					nombre[j] = buf[i];
					j++;
			}
			nombre[j] = 0;

			//borramos al jugador de la lista de jugaores, y tambi�n borramos sus datos de las otras listas (ip y puerto)
			for(j=0;j<10;j++){
				if(strcmp(lista_de_nombres[j],nombre)==0 ){
					strcpy(lista_de_nombres[j],"\0");
					strcpy(lista_de_ip[j],"\0");
					strcpy(lista_de_puertos[j],"\0");
					break;
				}
			}
			
			//comprobar si hay m�s jugadores a partir del que vamos a borrar para asignarles su nueva posicion en la lista
			if (j < num_clientes){			  	
				for(;j<num_clientes; j ++){
					memset(lista_de_nombres[j],'\0',40);
					memset(lista_de_ip[j],'\0',15);
					memset(lista_de_puertos[j],'\0',15);			  	
					strcpy(lista_de_nombres[j], lista_de_nombres[j+1]);
					strcpy(lista_de_ip[j], lista_de_ip[j+1]);
					strcpy(lista_de_puertos[j], lista_de_puertos[j+1]);
			  	}			  	 
			  	memset(lista_de_nombres[j],'\0',40);
			 	memset(lista_de_ip[j],'\0',15);
			  	memset(lista_de_puertos[j],'\0',15);	
			}					
			num_clientes--;			
			
			//impresi�n del nombre del usuario que se acaba de desconectar
			printf("Se ha desconectado del servidor: %s\n",nombre);		
			
			//envio del OK 
			sprintf(buf,"OK");
			if(sendto(sock, buf, strlen(buf), 0, (struct sockaddr *) &dir_cli, tam_dir) < 0){
				perror("Error al enviar datos.\n");
				exit(1);
			}
			
						    /*********************
						      borrar 2 jugadores
						    *********************/
		}else if(strcmp(comando, "PLA2") == 0){
			char nombre_del_Retador[strlen(buf+5)]; //variable que usaremos para guardar el nombre del jugador retador.
			char nombre_del_Retado[strlen(buf+5)];  //variable que usaremos para guardar el nombre del jugador retado.
			strcpy(nombre_del_Retador, buf+5);
			j=0;
			
			//cogemos el nombre del usuario retador.
			for (i = 5; i < n; i ++){
				if (buf[i]=='&') break;
				nombre_del_Retador[j] = buf[i]; 
				j++;
			}
			nombre_del_Retador[j-1] = 0 ;
					   		           
			//cogemos el nombre del usuario retado.
			strcpy(nombre_del_Retado, buf+i+1);
			nombre_del_Retado[n - i] = 0 ;		
		
		  	//impresi�n de los nombres de los jugadores que van a empezar a jugar y los cuales se borrar�n de la lista.
		  	printf("%s y %s empezaran a jugar en breves, por lo cual seran borrados de la lista.\n", nombre_del_Retador, nombre_del_Retado);
		
			//borramos al jugador retador de la lista de jugadores, y tambi�n borramos sus datos de las otras listas (ip y puerto)
			for(j=0;j<10;j++){
				if(strcmp(lista_de_nombres[j],nombre_del_Retador)==0 ){
					strcpy(lista_de_nombres[j],"\0");
					strcpy(lista_de_ip[j],"\0");
					strcpy(lista_de_puertos[j],"\0");
					break;
				}
			}
			
			//comprobar si hay m�s jugadores a partir del que vamos a borrar, en caso afirmativo asignarles su nueva posicion en la lista
			if (j < num_clientes){
				for(;j<num_clientes; j ++){
					memset(lista_de_nombres[j],'\0',40);
					memset(lista_de_ip[j],'\0',15);
					memset(lista_de_puertos[j],'\0',15);
					strcpy(lista_de_nombres[j], lista_de_nombres[j+1]);
					strcpy(lista_de_ip[j], lista_de_ip[j+1]);
					strcpy(lista_de_puertos[j], lista_de_puertos[j+1]);
			  	}			  	 
			 	memset(lista_de_nombres[j],'\0',40);
			 	memset(lista_de_ip[j],'\0',15);
				memset(lista_de_puertos[j],'\0',15);	
			}					
			num_clientes--;		
			
			//borramos al jugador retador de la lista de jugaores, y tambi�n borramos sus datos de las otras listas (ip y puerto)
			for(j=0;j<10;j++){
				if(strcmp(lista_de_nombres[j],nombre_del_Retado)==0 ){
					strcpy(lista_de_nombres[j],"\0");
					strcpy(lista_de_ip[j],"\0");
					strcpy(lista_de_puertos[j],"\0");
					break;
				}
			}
			
			//comprobar si hay m�s jugadores a partir del que vamos a borrar para asignarles su nueva posicion en la lista
			if (j < num_clientes){			  	
				  for(;j<num_clientes; j ++){
				  		memset(lista_de_nombres[j],'\0',40);
				  		memset(lista_de_ip[j],'\0',15);
				  		memset(lista_de_puertos[j],'\0',15);				  	
				  		strcpy(lista_de_nombres[j], lista_de_nombres[j+1]);
				  		strcpy(lista_de_ip[j], lista_de_ip[j+1]);
				  		strcpy(lista_de_puertos[j], lista_de_puertos[j+1]);
				  }			  	 
			  	  memset(lista_de_nombres[j],'\0',40);
			 	  memset(lista_de_ip[j],'\0',15);
			  	  memset(lista_de_puertos[j],'\0',15);	
			}					
			num_clientes--;						
		}else{
			printf("Ha hab�do un error a la hora de comparar el comando, esto puede ser debido a compilador que estes usando\n");
		}
	}//end while
	
	close(sock);
}
