#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "auxiliares.h"
#include <netdb.h>

#define MAX_BUF 1024
#define PORT 50005
#define MAX_WAIT 1000
#define PORT2 25005

//funciones definidas en auxiliares.h
extern void imprimir_menu(int longitud, char *opmenu);
extern int imprimir_listaJugadores(int longitud, char *listajug); 
extern int juego(char pos[4][4]);
extern int aceptar_movimiento(int x, int y, char pos[4][4]);
extern void imprimir_ganador();
extern void imprimir_perdedor();
extern void imprimir_tablas();


int main(int argc, char *argv[]){	
  //Variables de uso dinámico
  int i,j,k,z,n;
  int comando=0;
  char aux[10];
  char aux2[10];
  char buf[MAX_BUF];
  char buf2[MAX_BUF];			
  char coord_x[3];
  char coord_y[3];
  char send[5];
  
  //Variables fijas 
  char posiciones[4][4]={"   ","   ","   ","   "};//Para el juego.
  int puertoPropio;              //El puerto de cada cliente que se le asignará mediante la función bind.
  int puertoExternoA;            //Puerto del cliente retado.
  int puertoExternoB;            //Puerto del cliente retador.
  int sockEnvio,sockRecep;
  int conectado=0;               //Nos indica que el usuario acaba de conectarse al servidor.
  int jugar=0;                   //Variable que indica si un jugador se encuentra visualizando la lista de jugadores a punto de retar a otro jugador(1), y 
                                 //El otro se encuentra en espera de ser retado(0). 
  int jugador = 0;               //Indica si es el retado (0) o el retador  (1).
  int jugadas=0;                 //Nº de jugadas entre los jugadores.
  char nombre[MAX_BUF];          //Nombre del usuario que usa este cliente.
  char MENU[MAX_BUF];            //Variable para guardar el menu que nos envía el servidor.
  int longMenu=0;                 
  char LISTA[MAX_BUF];           //Variable para guardar la lista de jugadores que nos envía el servidor.
  int longLista=0;
  char IPS[MAX_BUF];             //Variable para guardar la lista de direcciones ip's que nos envía el servidor.
  char PUERTOS[MAX_BUF];         //Variable para guardar la lista de puertos que nos envía el servidor.
  socklen_t tam_dir, tam_dir2;
  fd_set rset;
  char ficha ;                    //Será un circulo o una equis (O o X)
  struct timeval timer;
  struct sockaddr_in dir_serv;   //Para la comunicación con el servidor
  struct sockaddr_in cli_jugador;//Para la comunicación entre los clientes
  struct sockaddr_in dir_nu;     //Para el socket de escucha, nuestra dirección
  struct sockaddr_in prueba;     //Auxiliar para guardar el puerto disponible que nos asigna el bind().
  struct hostent *hp;
  
  
  if(argc != 2){
   fprintf(stderr, "Uso: %s <DireccionIPv4>\n", argv[0]);
   exit(1);
  }

  //Socket para la comunicación con el servidor y con el cliente
  if((sockEnvio = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
	  perror("1º socket. Error al crear el socket.\n");
	  exit(1);
  }
  memset(&dir_serv, 0, sizeof(dir_serv));
  dir_serv.sin_family = AF_INET;
  dir_serv.sin_port = htons(PORT);
  if((hp = gethostbyname(argv[1])) == NULL){
		herror("Error al resolver el nombre del servidor");
		exit(1);
	}
	memcpy(&dir_serv.sin_addr, hp->h_addr, hp->h_length);

  //Socket para la comunicación con un posible jugador, este socket estará atendiendo peticiones de retos.
  if((sockRecep = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
	  perror("2º socket. Error al crear el socket.\n");
	  exit(1);
  }
  memset(&dir_nu, 0, sizeof(dir_nu));
  dir_nu.sin_family = AF_INET;
  dir_nu.sin_addr.s_addr = htonl(INADDR_ANY);  
  dir_nu.sin_port = htons(0);//Puerto libre para usar.
  
  if(bind(sockRecep, (struct sockaddr *) &dir_nu, sizeof(dir_nu)) < 0){ //La función bind nos asingará un puerto disponible.
    perror("2º socket. Error al asignarle una direccion al socket.\n");
	  exit(1);
  }
  tam_dir2 = sizeof(prueba);
  getsockname(sockRecep, (struct sockaddr *) &prueba, &tam_dir2);
  puertoPropio =  ntohs(prueba.sin_port);
  
  //Pedimos el nombre del usuario.
  printf("Por favor, escriba su nombre de usuario...\n");
  while(strlen(nombre)<5 || strlen(nombre)>40){
    if( fgets(nombre, MAX_BUF, stdin) == NULL){
	    fprintf(stderr, "Error en la lectura del nombre de usuario.\n");
	    exit(1);
    }
    if(strlen(nombre)<5 || strlen(nombre)>40) {
      printf("La longitud del nombre debe ser mayor que 4 y menor de 40!!!\n");
      printf("Por favor, escriba de nuevo nombre de usuario...\n");
    }
  }
  
  //Le pedimos al usuario que se conecte.
  printf("Conéctate con el servidor para empezar a jugar escribiendo 'conectar', !animate¡\n");
	
  while(1){	 
 
    	timer.tv_sec = MAX_WAIT;
    	timer.tv_usec = 0;
    	FD_ZERO(&rset);
    	FD_SET(sockRecep,&rset);
		  FD_SET(0,&rset);   	
    	if((n=select(sockRecep+1,&rset,NULL,NULL,&timer))< 0 ){
    		fprintf(stderr,"Error a la espera de recibir el siguiente mensaje.\n");
    		exit(1);
    	}   	   	
    	if(n==0){
			fprintf(stderr, "Tiempo de espera agotado. ¡Has sido desconectado del sevidor!\n" );
			exit(0);
		  }
		  
    	if(FD_ISSET(0, &rset)){
    		if(fgets(buf, MAX_BUF, stdin) == NULL){
    		fprintf(stderr, "Error en la lectura del comando.\n");
    		exit(1);
    		}   	
    	/******************************************
    	Asociación de los comandos y sus números:
    		Número		Comando
    		  0          CONE
    		  1          LIST
    		  2          EXIT	
    		  3			 RECARGAR MENÚ
    		  4          PLAY(envio)
    	*******************************************/ 
    	   
    		//Si es el primer comando conectar o un único número del 1 al 9
    		if((strcmp(buf, "conectar\n") == 0) || ((int)buf[0]>48 && (int)buf[0]<58 && strlen(buf)==2) ){//no encontrado
    			if(strcmp(buf, "conectar\n") == 0){
    				comando=0;conectado=1;
    			}else{
    				//Para que el usuario no pueda ir directamente a la opción 4, la de jugar con otro jugador
    				//sin haber pasado previamente por la lista
    				if(atoi(buf)!=4){
    					comando = atoi(buf);
    				}else{
    					comando =200;
    				}				
    			}
    			if(conectado){
    				switch(comando){	 
    					case 0:
    									/****************************
    										  		CONECTAR
    									*****************************/   
    									
    						if( sprintf(buf,"CONE_%s&%d",nombre,puertoPropio) < 0){
    							fprintf(stderr, "Error en la conversión del comando CONE a enviar.\n");
    							exit(1);
    						} 		
    						if(sendto(sockEnvio, buf, strlen(buf),0,(struct sockaddr *) &dir_serv, sizeof(dir_serv))<0)
    						{
    							perror("Error al enviar datos.\n");
    							exit(1);
    						}		
    						if((n = recvfrom(sockEnvio, buf, MAX_BUF,0,NULL,NULL)) < 0)
    						{
    							perror("Error al recibir datos.\n");
    							exit(1);
    						}
    						buf[n]=0;
    						strcpy(MENU,buf + 3);
    						longMenu=n-1;  
    						//Filtrar el OK o el ER de la respuesta
    						if(buf[0] == 'E' && buf[1]=='R'){ 
    							fprintf(stderr, "Error.\n");
    							exit(1);
    						}else{
    							imprimir_menu(longMenu,MENU);
    						}
    					break;
    					case 1:
    									/****************************
    									      LISTA DE JUGADORES
    									*****************************/    
                
                //Vaciamos siempre el contenido de la lista
    					  memset(LISTA,'\0',MAX_BUF);	
    					
    						if(sprintf(buf,"LIST") < 0){
    							fprintf(stderr, "Error en la conversión del comando LIST a enviar.\n");
    							exit(1);
    						}     
    						if(sendto(sockEnvio, buf, strlen(buf), 0,(struct sockaddr*)&dir_serv,sizeof(dir_serv)) < 0)
    						{
    							perror("Error al enviar datos.\n");
    							exit(1);
    						}  
    						/*Recibir la lista de jugadores del servidor.*/					   
    						if((k = recvfrom(sockEnvio, buf, MAX_BUF,0,NULL,NULL)) < 0)
    						{
    							perror("Error al recibir datos.\n");
    							exit(1);
    						}
    						//Filtrar el OK o el ER de la respuesta
    						if(buf[0] == 'E' && buf[1]=='R'){ 
    							fprintf(stderr, "Error.\n");
    							exit(1);
    						}
    						buf[k]=0;		
    						    						
    						printf("esto es lo que hay en el bufer: %s\n",buf);
    						strcpy(LISTA,buf+3);
    						longLista=k-2;
    						//Enviar mensaje de OK al servidor.
    						if( sprintf(buf,"OK") < 0){
    							fprintf(stderr, "Error en la conversión del comando OK a enviar.\n");
    							exit(1);
    						} 
    						if(sendto(sockEnvio, buf, strlen(buf),0,(struct sockaddr*) &dir_serv,sizeof(dir_serv)) < 0){
    							perror("Error al enviar datos.\n");
    							exit(1);
    						} 
    							  
    						/*Recibir la lista de ip's asociadas a cada jugador.*/
    						if((k = recvfrom(sockEnvio, buf, MAX_BUF,0,NULL,NULL)) < 0){
    							perror("Error al recibir datos.\n");
    							exit(1);
    						}
    						//Filtrar el OK o el ER de la respuesta
    						if(buf[0] == 'E' && buf[1]=='R'){ 
    							fprintf(stderr, "Error.\n");
    							exit(1);
    						}		
    						buf[k]=0;
    						strcpy(IPS,buf+3);
    						//Enviar mensaje de OK al servidor
    						if( sprintf(buf,"OK") < 0){
    							fprintf(stderr, "Error en la conversión del comando OK a enviar.\n");
    							exit(1);
    						} 
    						if(sendto(sockEnvio,buf,strlen(buf), 0,(struct sockaddr *) &dir_serv, sizeof(dir_serv)) <0){
    							perror("Error al enviar datos.\n");
    							exit(1);
    						} 
    						   
    						/*Recibir la lista de puertos asociados a cada jugador*/
    						if((k = recvfrom(sockEnvio, buf, MAX_BUF,0,NULL,NULL)) < 0)	{
    							perror("Error al recibir datos.\n");
    							exit(1);
    						}
    						//Filtrar el OK o el ER de la respuesta
    						if(buf[0] == 'E' && buf[1]=='R'){ 
    							fprintf(stderr, "Error.\n");
    							exit(1);
    						}		
    						buf[k]=0;
    						strcpy(PUERTOS,buf+3);
    						
    						//Enviar mensaje de OK al servidor
    						if( sprintf(buf,"OK") < 0){
    							fprintf(stderr, "Error en la conversión del comando a enviar.\n");
    							exit(1);
    						} 
    						if(sendto(sockEnvio,buf,strlen(buf), 0,(struct sockaddr *) &dir_serv, sizeof(dir_serv)) <0){
    							perror("Error al enviar datos.\n");
    							exit(1);
    						}
    						    											
    						j = imprimir_listaJugadores(longLista, LISTA);
     						printf("¿ Te gustaría retar a alguno ? (s/n)\n");    						
    						if( fgets(buf2, MAX_BUF, stdin) == NULL){
    							fprintf(stderr, "Error en la lectura de la respuesta\n");
    							exit(1);
    						}    						
    						if( strcmp(buf2,"s\n") ==0){
    							printf("¿ A qué número de jugador te gustaría retar ?\n");
    							if( fgets(buf2, MAX_BUF, stdin) == NULL){
    								fprintf(stderr, "Error en la lectura de la respuesta.\n");
    								exit(1);
    							}
    							if(atoi(buf2) < j && (int)buf2[0]>48 && (int)buf2[0]<58 && strlen(buf2)==2){
    								printf("Enviando solicitud de reto al otro jugador, espere por favor...\n");
    								//Directos al case 4
    							}
							   else{    					
    								fprintf(stderr, "Número de jugador incorrecto, por favor vuelva a intentarlo. Escriba 'exit' para salir.\n");
    								j = imprimir_listaJugadores(longLista, LISTA);    								
    								while(fgets(buf2, MAX_BUF, stdin) != NULL){
    									if(strcmp(buf2,"exit\n")==0){
    										break;			
    									}
    									if(atoi(buf2) < j && (int)buf2[0]>48 && (int)buf2[0]<58 &&strlen(buf2)==2){		   
    										printf("Enviando solicitud de reto al otro jugador, espere por favor...\n");
    										break;
    										//Directos al case 4	
    									}else{
    										fprintf(stderr, "Número de jugador incorrecto, por favor vuelva a intentarlo. "
    										"Escriba 'exit' para salir.\n");    				
    										j = imprimir_listaJugadores(longLista, LISTA);
    									}
    								}
    								if(strcmp(buf2,"exit\n")==0){
    										break;			
    								}
    							}	
    						}else{
    							if(strcmp(buf2,"n\n") !=0){
    							  fprintf(stderr, "Respuesta incorrecta, vuelva a eleguir la opción del menú.\n");
    							}
    							imprimir_menu(longMenu,MENU);
    							break;	
    						}
    					case 4:
    									/************************************
    									     Envio de reto al otro jugador
    									*************************************/    						
    						nombre[strlen(nombre) - 1] = 0;
    						if(sprintf(buf,"PLAY_%s&%d",nombre, puertoPropio) < 0){
    							fprintf(stderr, "Error en la conversión del comando a enviar");
    							exit(1);
    						}
    						//Es el jugador retador.
    						jugador = 1; 

    						//Obtención de la ip del jugador al que vamos a retar.
    						k=strlen(IPS);
    						z=atoi(buf2);
    						if(z==1){
    							for(i=0;;i++){
    								if(IPS[i]=='&') break;
    								if(IPS[i]==0) break;
    							}
    							strncpy(aux,IPS,i);
    							aux[i]=0;
    						}else{
    							j=0;n=0;
    							for(i=0;i<k;i++){
    								if(IPS[i]=='&'){
    									n++;
    									j=i;
    									if(n == z-1) break;
    								}
    							}
    							i++;    							
    							for(j=0,n=i;;n++,j++){
    								if(IPS[n]=='&') break;
    								if(IPS[n]==0) break;
    							}
    							if(IPS[n]==0){
    								strncpy(aux,IPS+i,j);
    								aux[j]=0;
    							}else{    								
    								strncpy(aux,IPS+i,j);
    								aux[j]=0;
    							}
    						} 	   											   						
    						//Obtención del puerto del jugador al que vamos a retar.
    						k=strlen(PUERTOS);
    						z=atoi(buf2);
    						if(z==1){
    							for(i=0;;i++){
    								if(PUERTOS[i]=='&') break;
    								if(PUERTOS[i]==0) break;
    							}
    							strncpy(aux2,PUERTOS,i);
    							aux2[i]=0;
    						}else{
    							j=0;n=0;
    							for(i=0;i<k;i++){
    								if(PUERTOS[i]=='&'){
    									n++;
    									j=i;
    									if(n == z-1) break;
    								}
    							}
    							i++;    							
    							for(j=0,n=i;;n++,j++){
    								if(PUERTOS[n]=='&') break;
    								if(PUERTOS[n]==0) break;
    							}
    							if(PUERTOS[n]==0){
    								strncpy(aux2,PUERTOS+i,j);
    								aux2[j]=0;
    							}else{    								
    								strncpy(aux2,PUERTOS+i,j);
    								aux2[j]=0;
    							}  					
    						}
    						memset(&cli_jugador, 0, sizeof(cli_jugador));
    						cli_jugador.sin_family = AF_INET;
    						cli_jugador.sin_port= htons(atoi(aux2));
    						puertoExternoA = atoi(aux2);
    						
    						if(inet_aton(aux,&cli_jugador.sin_addr) <= 0){
    							fprintf(stderr,"Error en la direccion IP: %s\n", aux);
    							exit(1);
    						}
    						
    						//Envío de reto al otro jugador(cliente).
    						if(sendto(sockEnvio,buf,strlen(buf), 0,(struct sockaddr *) &cli_jugador, sizeof(cli_jugador)) < 0){
    							perror("Error al enviar datos.\n");
    							exit(1);
    						}
    						jugar=1;
    						imprimir_juego(posiciones);
    						printf("Enviada la petición al jugador, esperando primer movimiento...");
    						
    					break;
    					case 2:
    									/****************************
    									        Desconectarse
    									*****************************/    
    						printf("¿ De verdad desea desconectarse del servidor ? (s/n)\n");
    						if( fgets(buf, MAX_BUF, stdin) == NULL){
    							fprintf(stderr, "Error en la lectura de la respuesta de salida.\n");
    							exit(1);
    						}

    						if(strcmp(buf,"s\n")==0){
    							if( sprintf(buf2,"EXIT_%s",nombre) < 0){
      							fprintf(stderr, "Error en la conversión del comando a enviar.\n");
      							exit(1);
    							} 							

                  //Envío al servidor del comando EXIT y del nombre del usuario que se quiere desconectar
    							if(sendto(sockEnvio, buf2, strlen(buf2), 0,(struct sockaddr *)&dir_serv,sizeof(dir_serv)) < 0){
      							perror("Error al enviar datos.\n");
      							exit(1);
    							}

    							if((n = recvfrom(sockEnvio, buf, MAX_BUF,0,NULL,NULL)) < 0){
      							perror("Error al recibir datos.\n");
      							exit(1);
    							}
    							//Comprobar si ha habido algún error y retransmitir si es necesario
    							j=0;	
    							while(!j){
    								if(buf[0] == 'E' && buf[1]=='R'){ 
    									fprintf(stderr,"Error.\n");
    									printf("Volvemos a retransmitir...\n");
    									if(sendto(sockEnvio, buf2, strlen(buf), 0,(struct sockaddr *)&dir_serv,sizeof(dir_serv)) < 0){
    										perror("Error al enviar datos.\n");
    										exit(1);
    									}
    									if((n = recvfrom(sockEnvio, buf, MAX_BUF,0,NULL,NULL)) < 0)	{
    										perror("Error al recibir datos.\n");
    										exit(1);
    									}
    								}else{
    									j=1;
    								}
    							}	
    							printf("Se ha desconectado del servidor con exito, ¡Hasta la vista!\n");
    							conectado=0;
    							exit(0);
    						}else if(strcmp(buf,"n\n")==0){
    							printf("¿ Qué deseas hacer ?\n");
    							imprimir_menu(longMenu,MENU);
    						}else{
    							fprintf(stderr, "Respuesta incorrecta, vuelva a eleguir la opción del menú.\n");
    							imprimir_menu(longMenu,MENU);
    						}
    					break;
    					case 3:
    									/****************************
    									      RECARGAR EL MENÚ
    									*****************************/
    						imprimir_menu(longMenu,MENU);
    					break;
    					default:
    									/****************************
    									     	   ERRORES
    									*****************************/
    						if(comando==200){
      						comando=4;
      						fprintf(stderr, "Error, número de opción incorrecto: %d, por favor vuelva a intentarlo...\n", comando);
      					}
    					break;	
    				}
    			}
    			printf("\n\n");
    			if(!conectado){	
    				printf("¿ Aún no te has conectado ? No esperes más y conectate ya para jugar. Para ello deberás escribir la palabra " 						"'conectar'.\n");
    			}else{
    			  if (!jugar){
    				  printf("--------------------¿ Qué deseas hacer ?--------------------\n");
    				}
    			}  
    			 			
    		}else{//end if --> no es un único número o el primer comando conectar
    			if(!conectado){
    				printf("¿ Aún no te has conectado ? No esperes más y conectate ya para jugar. Para ello deberás escribir la palabra " 						"'conectar'.\n");
    			}else{
    				printf("Por favor, escribe la acción que deseas hacer correctamente.\n");	
    			}	
    		}	
        
    	}else{//Hemos recibido una petición de un jugador
    			
    		 /****** CASOS *********
    		 
    		 	PLAY (recepción)
    		 	PLA2
    		 	SEND
    		 	EXIT
    		 	
    		 ***********************/	
    		 
    		 jugadas=0;
    		 tam_dir = sizeof(cli_jugador);
    		 
    		 //recibimos informacion del retador o el retado
         if((n = recvfrom(sockRecep, buf, MAX_BUF,0,(struct sockaddr *) &cli_jugador, &tam_dir)) < 0) {
           perror("Error al recibir datos.\n");
           exit(1);
         }           
         buf[n] = 0; 
         strcpy(send, buf);
         send[4] = 0;       

         //envio al servidor de que van a empezar a jugar, si es distinto de play, significa que es él el que esta retando
         //y somos nosotros los que tenemos que avisar al servidor de que nos borre de la lista.
         if(strcmp(send,"PLAY") == 0){
           //este es el jugador retador
           jugador = 0;
		       //cogemos el nombre 
		       char nombre_del_Retador[strlen(buf+5)];    
		       strcpy(nombre_del_Retador, buf+5);
		       j=0;
		       for(i = 5; i < n; i ++){		          
		         if(buf[i]=='&') break;
		           nombre_del_Retador[j] = buf[i]; 
		           j++;
		         }
		       nombre_del_Retador[j] = 0 ;
	           		           
		       //cogemos el puerto por el cual el cliente retador estará a la espera
		       strcpy(aux, buf+i+1);
		       puertoExternoB = atoi(aux);
                                                             
		       //concatenamos el comando con  los nombres
		       if(sprintf(buf,"PLA2_%s&%s",nombre,nombre_del_Retador) < 0){
		         fprintf(stderr, "Error en la conversión del comando PLA2 a enviar.\n");
		         exit(1);
		       }
		           
		           
		       //envio al servidor del comando mas los nombres , no necesitamos respuesta.                    
		       if(sendto(sockEnvio, buf, strlen(buf),0,(struct sockaddr *) &dir_serv, sizeof(dir_serv))<0){
		         perror("Error al enviar datos.\n");
		         exit(1);
		       }
		       system("clear");                   
		       imprimir_juego(posiciones); 
		       
		       //El jugador retado es el que realiza el primer movimiento 
		       printf("Introduce la fila X: \n");    
		       if(fgets(coord_x, MAX_BUF, stdin) == NULL){
		         fprintf(stderr, "Error en la lectura.\n");
		         exit(1);
		       }	  
		       while(((int)coord_x[0] <= 48 || (int)coord_x[0] >= 52) || strlen(coord_x) != 2 ){ 		
				     printf("Por favor, vuelva a introducir la fila correctamente.\n");		        
				     if(fgets(coord_x, MAX_BUF, stdin) == NULL){
		           fprintf(stderr, "Error en la lectura.\n");
		           exit(1);
		         }	
		       }
		       printf("Introduce la columna Y: \n");                         
		       if(fgets(coord_y, MAX_BUF, stdin) == NULL){
		         fprintf(stderr, "Error en la lectura.\n");
		         exit(1);
		       }	   				        
		       while(((int)coord_y[0] <= 48 || (int)coord_y[0] >= 52) || strlen(coord_y) != 2 ){        		
				     printf("Por favor, vuelva a introducir la columna correctamente\n");
				     if(fgets(coord_y, MAX_BUF, stdin) == NULL){
		           fprintf(stderr, "Error en la lectura.\n");
		           exit(1);
		         }	
		       }
		       ficha = 'O';	           		                     
		       if(sprintf(buf,"SEND_%c&%c&%c",coord_x[0],coord_y[0],ficha) < 0){
		         fprintf(stderr, "Error en la conversión del comando a enviar.\n");
		         exit(1);
		       }  
		       //Decimos porqué puerto queremos envíar el movimiento al cliente retador             
		       cli_jugador.sin_port = htons(puertoExternoB);                      
		       //envio al retador del primer movimiento. SEND_1_2_X
		       if(sendto(sockEnvio, buf, strlen(buf),0,(struct sockaddr *) &cli_jugador, sizeof(cli_jugador))<0){
		         perror("Error al enviar datos.\n");
		         exit(1);
		       }
		       sleep(1);
		       system("clear"); 
		       posiciones[atoi(coord_x)][atoi(coord_y)] = ficha; 
		       jugadas++;                       
         }else{//Ha recibido un SEND            
           ficha = 'X';
           system("clear");
           sprintf(coord_x,"%c",buf[5]);
           sprintf(coord_y,"%c",buf[7]);
           posiciones[atoi(coord_x)][atoi(coord_y)] = 'O';
           jugadas++;                           
         }
             
         //Hay que hacer el select, el último cliente que haya recibido un movimiento será al que le toque hacer el siguiente, mientras 				que el otro estará a la espera.
         while(1){
         	 imprimir_juego(posiciones);	
           if(strcmp(send,"SEND") == 0){                               
             //Introducir un movimiento válido.
             while(1){                   
				       printf("Introduce la fila X: \n");    
						   if(fgets(coord_x, MAX_BUF, stdin) == NULL){
						     fprintf(stderr, "Error en la lectura.\n");
						     exit(1);
						   }	
						   while(((int)coord_x[0] <= 48 || (int)coord_x[0] >= 52) || strlen(coord_x) != 2 ){ 		
								 fprintf(stderr, "Por favor, vuelva a introducir la fila correctamente.\n");
								 if(fgets(coord_x, MAX_BUF, stdin) == NULL){
						       fprintf(stderr, "Error en la lectura.\n");
						       exit(1);
						     }	
						   }
						   printf("Introduce la columna Y: \n");                         
						   if(fgets(coord_y, MAX_BUF, stdin) == NULL){
						     fprintf(stderr, "Error en la lectura.\n");
						     exit(1);
						   }	   
						   while(((int)coord_y[0] <= 48 || (int)coord_y[0] >= 52) || strlen(coord_y) != 2 ){        		
								 fprintf(stderr, "Por favor, vuelva a introducir la columna correctamente.\n");
								 if(fgets(coord_y, MAX_BUF, stdin) == NULL){
						       fprintf(stderr, "Error en la lectura.\n");
						       exit(1);
						     }	
						   } 
						   z = aceptar_movimiento(atoi(coord_x), atoi(coord_y), posiciones); 
						   
						   if(z == 0) break;    
					       fprintf(stderr, "No puedes poner la ficha en esa posición, ya está elegida, por favor"						 								
					       " elige otro movimiento.\n");					     		
		         }          

             //concatenación del comando con el movimiento.
             if(sprintf(buf,"SEND_%c&%c&%c",coord_x[0],coord_y[0],ficha) < 0){
               fprintf(stderr, "Error en la conversión del comando SEND a enviar.\n");
               exit(1);
             }              
             if(jugador==0){
               cli_jugador.sin_port = htons(puertoExternoB);
             }else{
               cli_jugador.sin_port = htons(puertoExternoA);
             }        
             //Envío del movimiento al cliente que se encuentra en espera.                         
             if(sendto(sockEnvio, buf, strlen(buf),0,(struct sockaddr *) &cli_jugador, sizeof(cli_jugador))<0){
               perror("Error al enviar datos.\n");
               exit(1);
             }
             sprintf(coord_x,"%c",buf[5]);
             sprintf(coord_y,"%c",buf[7]);
             posiciones[atoi(coord_x)][atoi(coord_y)] = ficha;
             jugadas++;
             imprimir_juego(posiciones);
                         
             if(jugadas>4){
               k= juego(posiciones);
		           if(k == 0){
		          	 system("clear");
		             imprimir_juego(posiciones);  
		           }else if(k==1){
		             imprimir_juego(posiciones);
		             sleep(2);
		             if(ficha == 'X') imprimir_ganador();
		             else imprimir_perdedor();
		             exit(0);
		           }else if(k==2){
		             imprimir_juego(posiciones);
		             sleep(2);
		             if(ficha=='O') imprimir_ganador();
		             else imprimir_perdedor();
		             exit(0);
		           }else{
		             imprimir_juego(posiciones);
		             sleep(2);
		             imprimir_tablas();
		             exit(0);	              		
		           }
             }          
           }                  
           printf("Movimiento enviado. Esperando al otro jugador...\n");    
           timer.tv_sec = MAX_WAIT;
           timer.tv_usec = 0;
           FD_ZERO(&rset);
           FD_SET(sockRecep,&rset); 
           
           //El cliente se queda a la espera de que le llegue un movimiento.         	
           if((n=select(sockRecep+1,&rset,NULL,NULL,&timer))< 0 ){
             fprintf(stderr,"Error a la espera de recibir el siguiente mensaje.\n");
             exit(1);
           }
           //El cliente recibe un movimiento.                           
           if((n = recvfrom(sockRecep, buf, MAX_BUF,0,(struct sockaddr *) &cli_jugador, &tam_dir)) < 0) {
             perror("Error al recibir datos.\n");
             exit(1);
           }              
           buf[n] = 0;
           strcpy(send, buf);
           send[4] = 0;             
           system("clear");
           sprintf(coord_x,"%c",buf[5]);
           sprintf(coord_y,"%c",buf[7]);
           posiciones[atoi(coord_x)][atoi(coord_y)] = buf[9]; 
           jugadas++;
           
           //A partir del 5 movimiento es donde puede que haya un posible ganador, dependiendo del caso se imprimirá el tablero y el 					resultado final.
		       if(jugadas>4){  
		         k = juego(posiciones); 
		         if(k==0){
		         	 system("clear");
		           imprimir_juego(posiciones);  
		         }else if(k==1){
		           imprimir_juego(posiciones);
		           sleep(2);
		           if(ficha=='X') imprimir_ganador();
		             else imprimir_perdedor();
		             exit(0);
		         }else if(k==2){
		           imprimir_juego(posiciones);
		           sleep(2);
		           if(ficha=='O') imprimir_ganador();
		           else imprimir_perdedor();
		           exit(0);
		         }else{
		           imprimir_juego(posiciones);
		           sleep(2);
		           imprimir_tablas();
		           exit(0);
		         }
	         }        
                         
      	}//end while(juego)
      
      }
    			

	}//end while

	close(sockRecep);
	close(sockEnvio);
	exit(0);
}

