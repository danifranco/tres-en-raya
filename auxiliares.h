
/**************************************************************************
***									***		
***	FUNCIONES AUXILIARES PARA EL FUNCIONAMIENTO DE LA APLICACIÓN	***
***									***
**************************************************************************/

#define rojo  "\x1B[31m"
#define verde  "\x1B[32m"
#define amarillo "\x1B[33m"
#define azul  "\x1B[34m"
#define morado  "\x1B[35m"
#define cyan  "\x1B[36m"
#define blanco  "\x1B[37m"
#define normal  "\x1B[0m"
		
		
void imprimir_menu(int longitud, char *opmenu){
	int j=1;
	int k=0;
	int i;
	int y;
	/********************************** MENU *************************************/
	printf("\n\n");	
	printf("#################################################################\n");
	printf("###########################~~MENU~~##############################\n");
	printf("#################################################################\n");//65
	printf("##                                                             ##\n");
	printf("##                1- ");
	for(i=0; i < longitud -1 ;i++){

			if( opmenu[i] != '&' ){
				printf("%c",opmenu[i]);
				k++;
			}else{
				for(y=0;y<42 - k;y++){printf(" ");}printf("##");
				printf("\n");
				j++;
				printf("##                %d- ",j);
				k=0;
			}
	}
	for(y=0;y<43 - k;y++){printf(" ");}printf("##");
	printf("\n");
	printf("##                                                             ##\n");	
	printf("#################################################################\n");	
	printf("#################################################################\n");
	printf("#################################################################\n");
	printf("\n\n");	
	/******************************* END MENU *************************************/
}


int imprimir_listaJugadores(int longitud, char *listajug){
	int j=1;
	int i=0;
	/********************************** LISTA ************************************/
	printf("\n\n");	
	printf("º-----------------------Lista de jugadores------------------------º\n");
	printf("%d- ",j);
	j++;
	for(;i<longitud;i++){
		if(listajug[i] != '&'){
			printf("%c",listajug[i]);
		}else{
			printf("\n");
			printf("%d- ",j);
			j++;
		}
	}
	printf("\n");
	printf("º-----------------------------------------------------------------º\n");
	/******************************* END LISTA ************************************/
	return j;
}



void imprimir_juego(char pos[4][4]){
 
    printf("\n\n");
    printf("************************ TRES EN RAYA ********************************\n");
    printf("\n\n");
    printf("   Jugador 1                  |      |                   Jugador 2       \n");
    printf("      O                  %c    |  %c   |   %c                  X             \n",pos[1][1],pos[1][2],pos[1][3] );
    printf("                      ________|______|________                           \n");
    printf("                              |      |                                   \n");
    printf("                         %c    |  %c   |   %c                              \n",pos[2][1],pos[2][2],pos[2][3] );
    printf("                      ________|______|________                           \n");
    printf("                              |      |                                   \n");
    printf("                         %c    |  %c   |   %c                              \n",pos[3][1],pos[3][2],pos[3][3] );
    printf("                              |      |                                   \n");
    printf("\n\n");
    printf("**********************************************************************\n");
    printf("\n\n");

}

/** Algoritmo para determinar si se ha acabado la partida o no **/
int juego(char pos[4][4]){

	/*****************************************************
	**							RETURNS								 **
	**		 0 -> Aún no hay ningún ganador					 **
	** 	 1 -> Ganador el jugador de la ficha X			 **
	**		 2 -> Ganador el jugador de la ficha O			 **
	**		 3 -> Tablas											 **
	*****************************************************/
	
	if(  //Comprobar filas
		  (pos[1][1] == 'X' && pos[1][2] == 'X' && pos[1][3] == 'X') || 
		  (pos[2][1] == 'X' && pos[2][2] == 'X' && pos[2][3] == 'X') || 
		  (pos[3][1] == 'X' && pos[3][2] == 'X' && pos[3][3] == 'X') ||
		  //Comprobar columnas
		  (pos[1][1] == 'X' && pos[2][1] == 'X' && pos[3][1] == 'X') || 
		  (pos[1][2] == 'X' && pos[2][2] == 'X' && pos[3][2] == 'X') || 
		  (pos[1][3] == 'X' && pos[2][3] == 'X' && pos[3][3] == 'X') ||
		  //Comprobar diagonales
		  (pos[1][1] == 'X' && pos[2][2] == 'X' && pos[3][3] == 'X') || 
		  (pos[3][1] == 'X' && pos[2][2] == 'X' && pos[1][3] == 'X') ){
		   
		return 1;
	}
	
	if(  //Comprobar filas
		  (pos[1][1] == 'O' && pos[1][2] == 'O' && pos[1][3] == 'O') || 
		  (pos[2][1] == 'O' && pos[2][2] == 'O' && pos[2][3] == 'O') || 
		  (pos[3][1] == 'O' && pos[3][2] == 'O' && pos[3][3] == 'O') ||
		  //Comprobar columnas
		  (pos[1][1] == 'O' && pos[2][1] == 'O' && pos[3][1] == 'O') || 
		  (pos[1][2] == 'O' && pos[2][2] == 'O' && pos[3][2] == 'O') || 
		  (pos[1][3] == 'O' && pos[2][3] == 'O' && pos[3][3] == 'O') ||
		  //Comprobar diagonales
		  (pos[1][1] == 'O' && pos[2][2] == 'O' && pos[3][3] == 'O') || 
		  (pos[3][1] == 'O' && pos[2][2] == 'O' && pos[1][3] == 'O') ){
		   
		return 2;
	}
	if(  (pos[1][1] == 'X' || pos[1][1] == 'O') && (pos[1][2] == 'X' || pos[1][2] == 'O') && (pos[1][3] == 'X' || pos[1][3] == 'O') && 
		  (pos[2][1] == 'X' || pos[2][1] == 'O') && (pos[2][2] == 'X' || pos[2][2] == 'O') && (pos[2][3] == 'X' || pos[2][3] == 'O') &&
		  (pos[3][1] == 'X' || pos[3][1] == 'O') && (pos[3][2] == 'X' || pos[3][2] == 'O') && (pos[3][3] == 'X' || pos[3][3] == 'O') ){
		return 3;	
	}	
	
	return 0;
}

//Para aceptar o rechazar el movimiento del jugador
int aceptar_movimiento(int x, int y, char pos[4][4]){
	
	if(pos[x][y] == 'X' || pos[x][y] == 'O'){ 
	return 1;
	}			
	else{
	return 0;
	}
}


void imprimir_ganador(){
	system("clear");
	printf("%s _     _                                           _          _    _        \n",verde);
	printf("%s| |   | |                                         | |        | |  | |       \n",verde);
	printf("%s| |__ | | ____  ___     ____  ____ ____   ____  _ | | ___    | |  | |       \n",verde);
	printf("%s|  __)| |/ _  |/___)   / _  |/ _  |  _ \\ / _  |/ || |/ _ \\   |_|  |_|     \n",verde);
	printf("%s| |   | ( ( | |___ |  ( ( | ( ( | | | | ( ( | ( (_| | |_| |   _    _        \n",verde);
	printf("%s|_|   |_|\\_||_(___/    \\_|| |\\_||_|_| |_|\\_||_|\\____|\\___/   |_|  |_| \n",verde);
	printf("%s                      (_____|                                               \n",verde);
	printf("%s",normal);

}


void imprimir_perdedor(){
	system("clear");
	printf("%s _     _                                    _ _     _          _    _        \n",rojo);
	printf("%s| |   | |                                  | (_)   | |        | |  | |      \n",rojo);
	printf("%s| |__ | | ____  ___    ____   ____  ____ _ | |_  _ | | ___    | |  | |      \n",rojo);
	printf("%s|  __)| |/ _  |/___)  |  _ \\ / _  )/ ___) || | |/ || |/ _ \\   |_|  |_|    \n",rojo);
	printf("%s| |   | ( ( | |___ |  | | | ( (/ /| |  ( (_| | ( (_| | |_| |   _    _       \n",rojo);
	printf("%s|_|   |_|\\_||_(___/   | ||_/ \\____)_|   \\____|_|\\____|\\___/   |_|  |_| \n",rojo);
	printf("%s                      |_|															        \n",rojo);
	printf("%s",normal);
}


void imprimir_tablas(){
	system("clear");
	printf("%s _     _       _           _                              _           _         \n",azul);  
	printf("%s| |   | |     | |         (_)                            | |         | |        \n",azul); 
	printf("%s| |__ | | ____| | _   ____ _  ___     ____ _   _  ____ _ | | ____  _ | | ___    \n",azul); 
	printf("%s|  __)| |/ _  | || \\ / _  ) |/___)   / _  | | | |/ _  ) || |/ _  |/ || |/ _ \\   \n",azul);
	printf("%s| |   | ( ( | | |_) | (/ /| |___ |  | | | | |_| ( (/ ( (_| ( ( | ( (_| | |_| |  \n",azul);
	printf("%s|_|   |_|\\_||_|____/ \\____)_(___/    \\_|| |\\____|\\____)____|\\_||_|\\____|\\___/   \n",azul);
	printf("%s                                        |_|  												\n",azul);
                                        	                                     
	printf("%s 			                                      _    _   	\n",azul);
	printf("%s 			                         _           | |  | |		\n",azul);
	printf("%s			  ____ ____  ____   ____| |_  ____   | |  | |		\n",azul);
	printf("%s 			 / _  )    \\|  _ \\ / _  |  _)/ _  )  |_|  |_|		\n",azul);
	printf("%s			( (/ /| | | | | | ( ( | | |_( (/ /    _    _ 		\n",azul);
	printf("%s 			\\____)_|_|_|| |_// \\_||_|\\___)____)  |_|  |_|		\n",azul);
	printf("%s			            |_| 												\n",azul);
	printf("%s",normal);
}

